# RaspberryPi

Uitleg en quickstart Raspberry Pi

## Inleiding
De Raspberry Pi is een mini computer, gemaakt om het experimenteren met computers betaalbaar en toegankelijk te maken. De Raspberry Pi kost maar 8 tot 35 Euro, afhankelijk van het gekozen model. De processor is speciaal en lijkt meer op de processor in je smartphone of tablet, het geheugen is beperkt en er wordt gebruik gemaakt van SD kaartjes voor de harde schijf. Dit is onder anderen gedaan zodat de Raspberry weinig stroom verbruikt en het zelfs mogelijk is uren te draaien van een aantal AA batterijen. Voor meerdere projecten kan je afzonderlijke SD kaartjes inrichten, zonder dat je telkens alles moet wissen en installeren. Ook kan je op die manier met meerdere mensen een Raspberry Pi delen.

## Wat kan je er mee?
De Pi kan veel van wat een standaard desktop computer ook kan. Je kan je Pi bijvoorbeeld gebruiken voor het typen van een werkstuk, het draaien van een webserver, het spelen van Minecraft en het afspelen van muziek of video. Maar je kan nog veel meer met de Pi. Omdat de Pi zo klein is kan je deze ook inbouwen in projecten waar een gewone computer te groot voor is of te lastig van stroom te voorzien is. De Raspberry is handig van stroom te voorzien via micro USB, net als veel tablets en smartphones. Zo is de Pi gebruikt voor het bestuderen van de Stratosfeer, het maken van foto’s van schuwe dieren of als alarm-systeem voor vervelende broertjes of zusjes. De Pi is zo ontworpen dat je er relatief eenvoudig hardware op aan kan sluiten. Dit kunnen o.a. kleine motoren of sensoren voor het meten van temperatuur, licht, druk, etc zijn.

De Raspberry Pi wordt gemaakt in opdracht van de Raspberry Pi Foundation, die de opbrengst weer gebruikt om educatieve programma’s rondom de Raspberry Pi te financieren en ontwikkeling van software, onderwijs en hardware rondom de Raspberry Pi te stimuleren. De ontwikkelaars begonnen in 2006 al met het bedenken van de Raspberry Pi op de Camebridge Universiteit in Engeland. De eerste Pi’s werden in China gemaakt, om te kunnen voldoen aan de grote vraag bij de lancering van de Pi. Tegenwoordig worden de Pi’s in Groot Brittannië gemaakt, zie ook [deze link](https://www.youtube.com/watch?v=Tza6Hl8wSJ0) met filmpje (Engelstalig).

## Hardware
Raspberry Pi consument varianten hebben de volgende basis componenten:
1. Een gecombineerde ARM CPU/GPU
2. Micro USB voeding
3. GPIO pinnen
4. HDMI
5. USB
6. Audio uitgang
7. Composiet Video uitgang (bij B+/A+/Pi2 gecombineerd met Audio)
8. Netwerk (niet op Model A/A+)
9. Camera aansluiting
10. Display aansluiting
11. SD kaart (micro SD bij B+/A+/Pi2,3 en (W)Zero)

![](img/RaspberryBplusLegenda.png)